//
//  PinFactory.h
//  Project1
//
//  Created by Justin Tolman on 4/30/14.
//  Copyright (c) 2014 Justin Tolman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MapViewController.h"

@interface PinFactory : NSObject
@property (strong, nonatomic) NSManagedObjectContext *pinCache;
@property (strong, nonatomic) NSMutableArray *pinArray;
+ (PinFactory*)sharedInstance;
-(void)addPin:(NSString *)title latitude:(double)latitude longitude:(double)longitude color:(UIColor *)color desc:(NSString *)description type:(NSUInteger) typeNumber user:(BOOL)userEntry image:(UIImage*)img;
-(void)removePin:(NSUInteger)index;
-(void)setMapViewController:(MapViewController*)mvc;
-(void) netPinsJson:(NSString *)urlString;
-(CLLocationCoordinate2D)getCoordinates;
@end
