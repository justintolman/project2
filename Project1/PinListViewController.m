//
//  PinListViewController.m
//  Project1
//
//  Created by Justin Tolman on 4/30/14.
//  Copyright (c) 2014 Justin Tolman. All rights reserved.
//

#import "PinListViewController.h"
#import "PinFactory.h"

@interface PinListViewController (){
    PinFactory *_pinBuilder;
    NSArray *_pinArray;
}

@end

@implementation PinListViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _pinBuilder=[PinFactory sharedInstance];
    _pinArray=_pinBuilder.pinArray;
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void) viewDidAppear:(BOOL) animated {
    [self.tableView reloadData];
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setPinArray:(NSMutableArray*)array{
    _pinArray=array;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_pinArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Set the data for this cell:
    
    cell.textLabel.text = [[_pinArray objectAtIndex:indexPath.row] title];
    cell.detailTextLabel.text = @"More text";
    cell.imageView.image = [UIImage imageNamed:@"flower.png"];
    
    // set the accessory view:
    cell.accessoryType =  UITableViewCellAccessoryDisclosureIndicator;
    UIButton *deleteButton=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    deleteButton.frame = CGRectMake(200.0f, 5.0f, 75.0f, 30.0f);
    [deleteButton setTitle:@"delete" forState:UIControlStateNormal];
    [cell addSubview:deleteButton];
    [deleteButton addTarget:self action:@selector(deletePin:) forControlEvents:UIControlEventTouchUpInside];
    cell.textLabel.font=[ UIFont fontWithName: @"Arial" size: 12.0 ];
    return cell;
}

-(void)deletePin:(id)sender
{
    UITableViewCell *clickedCell = (UITableViewCell *)[[sender superview] superview];
    [_pinBuilder removePin:[[self.tableView indexPathForCell:clickedCell] row]];
    [self.tableView reloadData];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"listEditPin" sender:self];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"listEditPin"]) {
        NSUInteger index=[self.tableView indexPathForSelectedRow].row;
        ZSAnnotation *pin=[_pinArray objectAtIndex:index];
        [segue.destinationViewController setPinData:pin.title latitude:pin.coordinate.latitude longitude:pin.coordinate.longitude image:pin.image description:pin.subtitle pin:pin];
    }
}

@end
